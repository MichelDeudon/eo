def test_requirements():

    # Default python requirements
    import os
    import sys
    import json
    import math
    import warnings
    import glob
    import shutil
    import pathlib
    import datetime

    # ML requirements
    import tqdm
    import skimage
    import datetime
    import numpy
    import pandas
    import torch

    # GIS utils
    import fiona
    import shapely
    import rasterio
    import geopandas
    import openeo

    # Dataviz requirements
    import plotly
    import matplotlib

def test_GIS_utils(app_prefix="./"):
    ''' Test GIS util to get squared bounding box from center (degrees) and radius (meters).'''

    import os
    import sys
    sys.path.insert(0,os.path.dirname(app_prefix))
    sys.path.insert(0,os.path.dirname(os.path.join(app_prefix, 'src/')))

    import numpy as np
    from data import bbox_from_point
    x1, y1, x2, y2 = bbox_from_point(point=(40.0482, 26.3013), distance=1000)
    assert isinstance(x1, float) and isinstance(x2, float) and isinstance(y1, float) and isinstance(y2, float)
    assert np.abs(x1-x2) < 100 and np.abs(y1-y2) < 100 # difference smaller than 100
