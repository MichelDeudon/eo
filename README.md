# Earth Observation Tutorials - Sentinel2 with Python

## Description
Github repository to access ESA's [Sentinel-2](https://sentinel.esa.int/web/sentinel/missions/sentinel-2) satellite imagery, fuse images and detect changes automatically, for example to enhance images or count boat traffic 🛰 🛥 ☄ using spectral, spatial and temporal features.

## Installation ⛵
- Make sure you have Python and Git installed on your machine. Then clone this repository by clicking on the clone button (above the code)
- ```git clone ...``` will create a new folder called ```eo```
- Move into the new directory ```cd eo```.

#### Edit credentials
You need to register an account on [Copernicus Data Space](https://documentation.dataspace.copernicus.eu/Registration.html). You also need to register an account on [Mapbox](https://studio.mapbox.com/) for data [visualizations](https://plot.ly/python/mapbox-layers/). Copy-paste your ```mapbox_access_token``` in ```config/credentials.json```.

#### Build with pip/conda:
- Create a python>=3.6 environment, for example with [Anaconda](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html).
- Install dependencies with ```pip install -r requirements.txt ``` 
- Run ```jupyter notebook``` and open a notebook in your browser.

## Usage

### 1. Download images
- Download [Sentinel 2](https://sentinel.esa.int/web/sentinel/missions/sentinel-2) L2A products under `data/sentinel2/downloads` using [openeo](https://openeo.org/).
- See Notebook 1. 

### 2. Fuse images and color/enhance background ⛵
- Compute a boat free background image by fusion of NDWI images over time (median or max of image set).
- Fusing multiple images can remove clouds and enhance details, but also all mobile elements like boats in rivers, seas and oceans.
- See Notebook 2. 

### 3. Detect changes

#### 2020 Custom Script Contest
- BLUE: Background estimation, land/water segmentation with NVDI calculation.
- GREEN: Replaced by the NIR band.
- RED: Boat detection with (or without) a neural network.

![Model_Segmentation](img/Model_Segmentation.png)

#### Learning to detect and count boat traffic
- Edit data/aoi.json to annotate images with boat counts 📷.
- Run Notebook 3 to train and test model on labeled images. The model takes as input 1 or 2 channels (NIR and background NDWI). It embeds/encodes pixels and patches into a final heatmap to predict boat presence and counts.

![Model_Architecture](img/Model_Architecture.png)

##### Results
![Model_Demo](img/Model_Demo.png)
![Model_Eval](img/Model_Eval.png)

## Reference and links
- [ ] 📡 2020/05/06 CNES [SpaceGate Article](https://spacegate.cnes.fr/fr/covid-19-venise-sans-les-bateaux)
- [ ] 🛰️ 2020/04/15 ESA [Tweet](https://mobile.twitter.com/EO_OPEN_SCIENCE/status/1250367319936765953)
- [ ] 🇺🇳 2021-2030 UN [Decade on Ocean Science for Sustainable Development](https://www.oceandecade.org/)
- [ ] 🇺🇳 2015-2030 UN [SDG 14: Conserve and sustainably use the oceans, seas and marine resources](https://www.oceandecade.org/)

## Roadmap
- [ ] Add CI/CD (lint, test, black) .gitlab-ci.yml
- [ ] Write tuto Medium and mtpcours.fr (document code)
- [ ] Test Sentinel-1 and Tropomi download
- [ ] Automate analysis for AOI 🗺 ("EU Fisheries", "Ports", "Straits", "Tourism", "MPA")
- [ ] Fix bugs

## Authors and acknowledgment
[Michel Deudon](https://framagit.org/MichelDeudon)

[ESA](https://www.esa.int/), [VITO](https://remotesensing.vito.be/case/proba-v-0), [Copernicus](https://scihub.copernicus.eu/dhus/#/home)

## License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>

This repository is made available under the <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a> and the [Do No Harm License](https://github.com/raisely/NoHarm).