""" Pytorch implementation of a neural network to detect and count boats in Sentinel-2 imagery.
    Author: Michel DEUDON & Zhichao LIN """

import os
import torch
import torch.nn as nn
import numpy as np

def load_model(checkpoint_dir="../models/weights/boatnet/", version=1.0, input_dim=1, hidden_dim=32, kernel_size=5, pool_size=2):
    """
    Args:
        checkpoint_dir: str, path to checkpoint directory
        version: str, example '0.0.1'
    """
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu' # gpu support
    model = BoatNet(input_dim=input_dim, hidden_dim=hidden_dim, pool_size=pool_size, kernel_size=kernel_size, device=device, version=version) #####
    checkpoint_file = os.path.join(checkpoint_dir, model.folder, 'model.pth')
    model.load_checkpoint(checkpoint_file=checkpoint_file)
    model = model.eval()
    print('Loaded model version {} on {}'.format(version, device))
    return model
    
class BoatNet(nn.Module):
    ''' A neural network to detect and count boats in Sentinel-2 imagery '''

    def __init__(self, input_dim=1, hidden_dim=32, kernel_size=5, pool_size=2, drop_proba=0.01, pad=True, device='cuda:0', version=0.0):
        '''
        Args:
            input_dim : int, number of input channels. If 2, recommended bands: B08 + B03 or B08 + background NDWI.
            hidden_dim: int, number of hidden channels
            kernel_size: int, kernel size
            pool_size: int, pool size (chunk). Default 10 pixels (1 ha = 100m x 100m).
            drop_proba: int
            pad: bool, padding to keep input shape. Necessary for Residual Block (TODO).
            device: str, pytorch device
            version: float or str, model identifier
        '''
        
        super(BoatNet, self).__init__()
        self.folder = 'i{}_h{}_k{}_p{}_v{}'.format(input_dim, hidden_dim, kernel_size, pool_size, version)
        self.device = torch.device(device if torch.cuda.is_available() else 'cpu')
        self.pool_size = pool_size

        self.embed = nn.Sequential(
            nn.Conv2d(in_channels=input_dim, out_channels=hidden_dim, kernel_size=kernel_size, padding=int(pad)*kernel_size//2),
            nn.ReLU(),
            #nn.BatchNorm2d(hidden_dim),
        )
        #self.embed[0].weight = torch.nn.init.orthogonal_(self.embed[0].weight)
        
        self.residual = nn.Sequential(
            nn.Conv2d(in_channels=hidden_dim, out_channels=hidden_dim, kernel_size=3, padding=1),
            nn.ReLU(),
            #nn.Conv2d(in_channels=hidden_dim, out_channels=hidden_dim, kernel_size=3, padding=1),
            #nn.ReLU(),
        )
        
        self.residual2 = nn.Sequential(
            nn.Conv2d(in_channels=hidden_dim, out_channels=hidden_dim, kernel_size=3, padding=1),
            nn.ReLU(),
            #nn.Conv2d(in_channels=hidden_dim, out_channels=hidden_dim, kernel_size=3, padding=1),
            #nn.ReLU(),
        )
        
        
        self.residual3 = nn.Sequential(
            nn.Conv2d(in_channels=hidden_dim, out_channels=hidden_dim, kernel_size=3, padding=1),
            nn.ReLU(),
            #nn.Conv2d(in_channels=hidden_dim, out_channels=hidden_dim, kernel_size=3, padding=1),
            #nn.ReLU(),
        )
        
        self.dropout = nn.Dropout2d(p=drop_proba, inplace=False)
        self.max_pool = nn.MaxPool2d(pool_size, stride=pool_size)

        self.encode_patch = nn.Sequential(
            #nn.Conv2d(in_channels=hidden_dim, out_channels=hidden_dim, kernel_size=1, padding=0),
            #nn.PReLU(),
            nn.Conv2d(in_channels=hidden_dim, out_channels=1, kernel_size=1, padding=0),
            nn.Sigmoid(),
            #nn.Softmax(dim=1),
        )
        
        self.to(self.device)
        
    def forward(self, x, downsample=True):
        '''
        Predict boat presence (or counts) in an image x
        Args:
            x: tensor (B, C_in, H, W), satellite images [NIR, BG_NDWI, CLP]
            downsample: bool
        Returns:
            pixel_embedding: tensor (B, C_h, H, W), hidden tensor
            density_map: tensor (B, 1, H//pool_size, W//pool_size), boat density
            p_hat: tensor (B, 1), proba boat presence
            y_hat: tensor (B, 1), boat counts (expectation)
        '''
        
        x = x.to(self.device)
        batch_size, channels, height, width = x.shape 
        pixel_embedding = self.embed(x)
        pixel_embedding = self.dropout(pixel_embedding)
        pixel_embedding = self.residual(pixel_embedding) # h1 (B, C_h, H, W)
        pixel_embedding = self.max_pool(pixel_embedding)
        pixel_embedding = self.residual2(pixel_embedding) # h1 (B, C_h, H, W)
        pixel_embedding = self.max_pool(pixel_embedding)
        pixel_embedding = self.residual3(pixel_embedding) # h1 (B, C_h, H, W)
        pixel_embedding = self.max_pool(pixel_embedding)
        density_map = self.encode_patch(pixel_embedding) # density map (B, 1, H//pool_size, W//pool_size), pool_size res (10pix = 100m)

        # probability there is a boat or more (for each chunk)
        p_max = torch.max(torch.max(density_map, dim=-1).values, dim=-1).values # (B, 1) tight lower bound on probability there is a boat in full image
        p_med = torch.median(torch.median(density_map, dim=-1).values, dim=-1).values # (B, 1) tight lower bound on probability there is a boat in full image
        y_hat = torch.sum(density_map * (density_map>0.00001), (2,3)) # estimate number of boats in image (where there are boats)
        return density_map, p_max, p_med, y_hat
    
    def get_loss(self, x, y, downsample=True, ld=0.5, eps=0.000000001):
        '''
        Computes loss function for classification / regression (params: low-dim projection W + n_clusters centroids)
        Args:
            x: tensor (B, C, H, W), input images
            y: tensor (B, 1), boat counts or presence
            downsample: bool
            ld: float in [0,1], coef for count loss (SmoothL1) vs. presence loss (BCE)
            eps: float, for numerical stability (precision, recall)
        Returns:
            metrics: dict
        '''
        
        x = x.to(self.device)
        density_map, p_max, p_med, y_hat = self.forward(x, downsample=downsample)  # (B,1,n_filters,H,W)

        # compute loss
        p = 1.0*(y>0)
        criterion = torch.nn.BCELoss(reduction='mean')
        clf_error = criterion(p_max, p) + criterion(p_med, 0.*p) # loss for boat presence (proba vs. binary)
        criterion = torch.nn.SmoothL1Loss(reduction='mean') 
        reg_error = criterion(y_hat, y) # loss for boat counts (expected vs. label)
        loss = (1-ld)*clf_error + ld*reg_error
        
        # metrics for boat presence
        p_ = 1.0*(p_max>0.5)
        accuracy = (torch.mean(1.0*(p_==p)).detach()).cpu().numpy()
        precision = ((torch.sum(p_*p)+eps)/(torch.sum(p_)+eps)).detach().cpu().numpy()
        recall = ((torch.sum(p_*p)+eps)/(torch.sum(p)+eps)).detach().cpu().numpy()
        f1 = 2*precision*recall/(precision+recall)
        
        metrics = {'loss':loss, 'clf_error':clf_error, 'reg_error':reg_error, 'accuracy':accuracy, 'precision':precision, 'recall':recall, 'f1':f1}
        return metrics
    
    def load_checkpoint(self, checkpoint_file):
        '''
        Args:
            checkpoint_file : str, checkpoint file
        '''
        
        if not torch.cuda.is_available():
            self.load_state_dict(torch.load(checkpoint_file, map_location=torch.device('cpu')))
        else:
            self.load_state_dict(torch.load(checkpoint_file))
            
    def chip_and_count(self, x, downsample=False):
        """ Chip an image, predict presence for each chip and return heatmap of presence and total counts.
        Args:
            x: tensor (N, C_in, H, W)
            downsample: bool,
        Returns:
            heatmaps: list of np.array of size (H/chunk_size, W/chunk_size)
            counts: list of int
        """

        # process data by time (avoid memory overflow)
        heatmaps = []
        counts = []
        n_frames, channels, height, width = x.shape
        for t in range(n_frames):
            density_map, _, _, y_hat = self.forward(x[t:t+1].float(), downsample=downsample)
            density_map = density_map.detach().cpu().numpy()[0][0] # (H, W)
            y_hat = y_hat.detach().cpu().numpy()[0] # (1,)
            heatmaps.append(density_map)
            counts.append(float(y_hat))

        return heatmaps, counts
