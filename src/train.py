import os
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from tqdm.notebook import tqdm as tqdm
import torch
import torch.optim as optim

from model import BoatNet


def train(train_dataloader, val_dataloader, input_dim=1, hidden_dim=16, kernel_size=3, pool_size=2, drop_proba=0.01, ld=0.8, n_epochs=10, lr=0.001, lr_step=2, lr_decay=0.95, device='cpu', checkpoints_dir='./checkpoints', seed=42, version=0.0):
  """
  Trains a neural network for boat traffic detection.
  Args:
      train_dataloader: torch.Dataloader
      val_dataloader: torch.Dataloader
      input_dim: int, number of input channels
      hidden_dim: int, number of hidden channels
      kernel_size: int, kernel size
      pool_size: int, pool size (chunk). Default 2 pixels.
      drop_proba: float, 2D dropout probability.
      ld: float, coef for smooth count loss (sum, SmoothL1) vs. presence loss (max, BCE)
      n_epochs: int, number of epochs
      lr, lr_step, lr_decay: float and int for the learning rate
      device: str, 'cpu' or 'gpu'
      checkpoints_dir: str, path to checkpoints
      seed: int, random seed for reproducibility
      version: float or str
  """

  np.random.seed(seed)  # seed RNGs for reproducibility
  torch.manual_seed(seed)
  torch.backends.cudnn.deterministic = True
    
  model = BoatNet(input_dim=input_dim, hidden_dim=hidden_dim, kernel_size=kernel_size, pool_size=pool_size, drop_proba=drop_proba, device=device, version=version) 
  checkpoint_dir_run = os.path.join(checkpoints_dir, model.folder)
  os.makedirs(checkpoint_dir_run, exist_ok=True)
  print('Number of trainable params', sum(p.numel() for p in model.parameters() if p.requires_grad))

  best_metrics, best_score, best_epoch = {}, 100000., 0
  optimizer = optim.Adam(model.parameters(), lr=lr) # optim
  scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=lr_decay, patience=lr_step)
  for e in tqdm(range(n_epochs)):
    train_clf_error, train_reg_error, val_clf_error, val_reg_error = 0.0, 0.0, 0.0, 0.0
    train_accuracy, train_precision, train_recall, train_f1 = 0.0, 0.0, 0.0, 0.0
    val_accuracy, val_precision, val_recall, val_f1 = 0.0, 0.0, 0.0, 0.0
    
    for data in train_dataloader:
        model = model.train()
        optimizer.zero_grad()  # zero the parameter gradients
        X = data['img'].float()#.transpose(1,3)
        metrics = model.get_loss(X, y=data['y'].float().to(device), ld=ld)
        metrics['loss'].backward() # backprop
        optimizer.step()
        train_clf_error += metrics['clf_error'].detach().cpu().numpy()*len(data['img'])/len(train_dataloader.dataset)
        train_reg_error += metrics['reg_error'].detach().cpu().numpy()*len(data['img'])/len(train_dataloader.dataset)
        train_accuracy += metrics['accuracy']*len(data['img'])/len(train_dataloader.dataset)
        train_precision += metrics['precision']*len(data['img'])/len(train_dataloader.dataset)
        train_recall += metrics['recall']*len(data['img'])/len(train_dataloader.dataset)
        train_f1 += metrics['f1']*len(data['img'])/len(train_dataloader.dataset)
    
    for data in val_dataloader:
        model = model.eval()
        optimizer.zero_grad()  # zero the parameter gradients
        X = data['img'].float()#.transpose(1,3)
        metrics = model.get_loss(X, y=data['y'].float().to(device), ld=ld)
        val_clf_error += metrics['clf_error'].detach().cpu().numpy()*len(data['img'])/len(val_dataloader.dataset)
        val_reg_error += metrics['reg_error'].detach().cpu().numpy()*len(data['img'])/len(val_dataloader.dataset)
        val_accuracy += metrics['accuracy']*len(data['img'])/len(val_dataloader.dataset)
        val_precision += metrics['precision']*len(data['img'])/len(val_dataloader.dataset)
        val_recall += metrics['recall']*len(data['img'])/len(val_dataloader.dataset)
        val_f1 += metrics['f1']*len(data['img'])/len(val_dataloader.dataset)
        
    scheduler.step((1-ld)*val_clf_error+ld*val_reg_error)
    #if (1-ld)*val_clf_error+ld*val_reg_error<best_score:
    if val_reg_error<best_score:
        #best_score = (1-ld)*val_clf_error+ld*val_reg_error
        best_score = val_reg_error
        best_epoch = e+1
        best_metrics = {'best_epoch':best_epoch, 'train_clf_error': train_clf_error, 'train_reg_error':train_reg_error,
                        'val_clf_error':val_clf_error, 'val_reg_error':val_reg_error,
                       'train_accuracy':train_accuracy, 'train_precision':train_precision, 'train_recall':train_recall,
                        'train_f1':train_f1, 'val_accuracy':val_accuracy, 'val_precision':val_precision, 'val_recall':val_recall, 'val_f1':val_f1}
        torch.save(model.state_dict(), os.path.join(checkpoint_dir_run, 'model.pth'))
        print('Epoch {}: train_clf_error {:.5f} / train_reg_error {:.5f} / val_clf_error {:.5f} / val_reg_error {:.5f}'.format(best_epoch, train_clf_error, train_reg_error, val_clf_error, val_reg_error))
    
  return best_metrics

def test(model, dataloader):
    
    """ 
    Run model on dataset and display success or failures. Scatter plot predicted counts vs. true counts.
    Args:
        model: pytorch Model
        dataloader: torch.DataLoader
    """
    
    true = []
    pred = []

    for batch in dataloader:
        X = batch['img'] # (L,C,H,W)
        y = batch['y'] # (L,C,H,W)
        heatmaps, counts = model.chip_and_count(X, downsample=True) # Detect and count boats!
        heatmaps = np.stack(heatmaps,0)
        true.append(float(y[0,0].numpy()))
        pred.append(heatmaps.sum())
    
    # Scatter plot (Predicted vs True)
    plt.scatter(true,pred)
    plt.xlabel('true')
    plt.ylabel('pred')
    plt.title('Boat counts predictions')
    plt.show()
    
    return true, pred
