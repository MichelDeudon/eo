import os
import sys
import warnings
import glob
import math
import skimage
import pathlib
import torch
import openeo

import numpy as np
import pandas as pd
import rasterio as rio
import geopandas as gpd
import matplotlib.pyplot as plt
import plotly.express as px

from shapely.geometry import Point
from torch.utils.data import Dataset
from skimage.io import imsave, imread

if not sys.warnoptions:
    warnings.simplefilter("ignore")

def project_geometry(geometry, crs='+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs', to_crs=None, to_latlong=False):
    """
    Args:
        geometry: shapely Polygon or MultiPolygon, the geometry to project
        crs: dict or string or pyproj.CRS, the starting coordinate reference system of the passed-in geometry
        to_crs: dict or string or pyproj.CRS
        to_latlong : bool, if True, project from crs to lat-long, if False, project from crs to local UTM zone
    Returns:
        geometry_proj, crs: tuple, the projected shapely geometry and the crs of the projected geometry
    """

    gdf = gpd.GeoDataFrame()
    gdf.crs = crs
    gdf.gdf_name = 'geometry to project'
    gdf['geometry'] = None
    gdf.loc[0, 'geometry'] = geometry
    assert len(gdf) > 0, 'You cannot project an empty GeoDataFrame.'

    if to_crs is not None:
        gdf_proj = gdf.to_crs(to_crs)
    else:
        if to_latlong:
            latlong_crs = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs'
            gdf_proj = gdf.to_crs(latlong_crs) # project the gdf to latlong
        else:
            avg_longitude = gdf['geometry'].unary_union.centroid.x # calculate the centroid of the union of all the geometries in the GeoDataFrame
            utm_zone = int(math.floor((avg_longitude + 180) / 6.) + 1) # calculate the UTM zone from this avg longitude and define the UTM CRS to project
            utm_crs = '+proj=utm +zone={} +ellps=WGS84 +datum=WGS84 +units=m +no_defs'.format(utm_zone)
            gdf_proj = gdf.to_crs(utm_crs) # project the GeoDataFrame to the UTM CRS

    gdf_proj.gdf_name = gdf.gdf_name
    geometry_proj = gdf_proj['geometry'].iloc[0]
    return geometry_proj, gdf_proj.crs

def bbox_from_point(point, distance=1000):
    """
    Args:
        point: tuple (lat, lon) to create the bounding box around
        distance: int, how many meters the north, south, east, and west sides of the box should each be from the point
    Returns:
        north, south, east, west: tuple
    """

    # reverse the order of the (lat,lng) point so it is (x,y) for shapely, then project to UTM and buffer in meters
    lat, lng = point
    point_proj, crs_proj = project_geometry(Point((lng, lat)))
    buffer_proj = point_proj.buffer(distance)

    # project back to lat-long then get the bounding coordinates
    buffer_latlong, _ = project_geometry(buffer_proj, crs=crs_proj, to_latlong=True)
    west, south, east, north = buffer_latlong.bounds
    return north, south, east, west

def download_s2(bands=["B08", "B04", "B03", "B02"], start_date="2023-07-01", end_date="2023-10-23", lat_lon=(42.0,-93.0), radius=3000, max_cloud_percentage=10, prefix='../data/', lazy=True):
    """ 
    Download S2 data matching (lat,lon) + radius + time window.
    Preprocess downloaded products (B03, B08).
    Stack channels (RGB, NGB)
    Rescale intensity?? #####
    Save img as png.
      ex: '../data/sentinel2/snaps/Txxxxx/lon0_lat0/NIR/20190101.png'.
    Args:
      bands: List of str, default NRGB
      start_date: str, format yyyy-mm-dd
      end_date: str, format yyyy-mm-dd
      lat_lon: tuple or list (latitude, longitude)
      radius: int, region of interest      
      max_cloud_percentage: float in [0,100]
      prefix: str, path to data
    """

    # Authenticate to your account to download files (provide credentials, replace data)
    connection = openeo.connect(url="openeo.dataspace.copernicus.eu")
    connection.authenticate_oidc() 

    # Retrieve BBox from (lat,lon) and radius
    north, south, east, west = bbox_from_point(lat_lon, distance=radius)

    # Create datacube
    cube = connection.load_collection(
            "SENTINEL2_L2A", ##### :param dataset_name: either S2L1C or S2L2A
            bands=bands, # keep NIR + RGB (4 bands)
            temporal_extent=(start_date, end_date),
            spatial_extent={
                "west": west,
                "south": south,
                "east": east,
                "north": north,
                "crs": "EPSG:4326",
            },
            max_cloud_cover=max_cloud_percentage, # filtering by cloud coverage
        )

    # Create download directories
    s2_download_path = os.path.join(prefix,'sentinel2/downloads')
    os.makedirs(s2_download_path, exist_ok=True)

    # Skip if products already downloaded
    data_path = os.path.join(prefix, "sentinel2/downloads/lat{}_lon{}/".format(int(lat_lon[0]*10000), int(lat_lon[1]*10000)))
    results = [path.name.split("_")[-1][:10] for i, path in enumerate(sorted(pathlib.Path(data_path).glob("*tif")))] # tif files already on disk
    if len(results)==0 or lazy is False:

        # Set up output format to be GeoTIFF
        #cube = cube.dropna(dim='time',how=nans_how) # drop images w/ any nan 
        cube = cube.save_result(format="GTiff")

        # Run as a batch job
        job = cube.execute_batch(title="Slice of S2 data {} {} {}".format(lat_lon, start_date, end_date))

        # Fetch batch job results
        results = job.get_results()

        # Save results (.GTiff)
        os.makedirs(data_path, exist_ok=True)
        results.download_files(data_path)


    if bands == ["B08", "B04", "B03", "B02"]:

        # Save results as .png
        for i, path in enumerate(sorted(pathlib.Path(data_path).glob("*tif"))):
            nrgb = rio.open(path).read() # (C,H,W)
            filename = path.name.split("_")[-1][:10]

            # Convert to numpy arrays, stack bands and normalize
            nrgb = np.dstack((nrgb[0], nrgb[1], nrgb[2], nrgb[3])) # (H,W,C)

            # Save RGB color composite
            rgb = (skimage.exposure.rescale_intensity(nrgb[:,:,1:4], out_range='float')*255).astype(np.uint8)
            nrg = (skimage.exposure.rescale_intensity(nrgb[:,:,0:3], out_range='float')*255).astype(np.uint8)

            ##### save NDWI background to GTiff
            # NDWI, reference (McFeeters, 1996)
            #ndwi = torch.from_numpy(np.stack((lrs[:,2:3]-lrs[:,0:1])/(lrs[:,2:3]+lrs[:,0:1]),0))
            #ndwi = torch.max(ndwi, 0).values
            #ndwi = (ndwi+1.0)/2.0 # from [-1,1] to [0,1]
            #ndwi = 1.0 * (ndwi.numpy() > 0.)
            #background_ndwi = ndwi.min(dim='time')
            #imsave(os.path.join(data_dir, subdir, 'bg_ndwi.png'), background_ndwi.values)

            # Cloud detector, reference (Braaten-Cohen-Yang, 2015)
            #cloud = lrs/9960.
            #cloud = 1.0*torch.from_numpy(np.stack((cloud[:,2:3]>0.175) *(cloud[:,2:3]>cloud[:,1:2])+(cloud[:,2:3]>0.39),0))
            # sort_values, cloud_percentage

            imsave(os.path.join(data_path,'RGB_{}.png'.format(filename)), rgb) # img is saved on 8 bytes
            imsave(os.path.join(data_path,'NRG_{}.png'.format(filename)), nrg)

    return results

def build_s2_imset(lat_lon=(42.0,-93.0), prefix='../data/'):
    """    
    Builds a multi temporal, hyperspectral (NRGB) tensor from an S2 tile.
    Args:
        prefix: str, path to data
    Returns:
        imset: dict, an ImageSet for the given index (int).
    """

    imset = {}
    data_path = os.path.join(prefix, "sentinel2/downloads/lat{}_lon{}/".format(int(lat_lon[0]*10000), int(lat_lon[1]*10000)))

    filenames = [path.name for i, path in enumerate(sorted(pathlib.Path(data_path).glob("*tif")))] # list of filenames
    images = [rio.open(path).read() for i, path in enumerate(sorted(pathlib.Path(data_path).glob("*tif")))] # bands: "NDWI", "NIR", "RGB"
    images = np.stack(images) #/255. # in [0,1]
    imset["img"] = torch.from_numpy(images) # (L,1,H,W)
    imset["timestamp"] = [name.split('_')[-1][:10] for name in filenames]
    return imset

def plot_geoloc(train_coordinates, val_coordinates=None, mapbox_access_token=None):
    """
    Args:
        train_coordinates: list of (lat,lon)
        val_coordinates: list of (lat,lon)
        mapbox_access_token: Mapbox access token
    Returns:
        MapboxPlot of region
    """

    if val_coordinates is not None:
        df = pd.DataFrame(train_coordinates+val_coordinates, columns=['lat', 'lon'])
        df.insert(2, "color", [0]*len(train_coordinates)+[1]*len(val_coordinates), True)
    else:
        df = pd.DataFrame(train_coordinates, columns=['lat', 'lon'])
        df.insert(2, "color", [0]*len(train_coordinates), True)
    px.set_mapbox_access_token(mapbox_access_token)
    if val_coordinates is not None:
        fig = px.scatter_mapbox(df, lon="lon", lat="lat", color='color', zoom=2, color_continuous_scale=px.colors.sequential.Bluered, width=1024, height=768)
    else:
        fig = px.scatter_mapbox(df, lon="lon", lat="lat", color='color', zoom=2, color_continuous_scale=px.colors.sequential.Greens, width=1024, height=768)
    return fig

def plot_from_dict(image_dict, figsize=(15,5), cmap=None):
  '''
  Args:
      image_dict: dict of (title, np.array)
  '''
  n = len(image_dict)
  n_rows = 1+(n-1)//2
  fig = plt.figure(figsize=figsize) #, figsize=(10, 7))
  for i,(k,v) in enumerate(image_dict.items()):
    plt.subplot(n_rows,2,1+i)
    if cmap is None or len(v.shape)==3:
      v = skimage.exposure.rescale_intensity(v, out_range='float')
      plt.imshow(v)
    elif 'change' in k.lower():
      plt.imshow(v, cmap='magma');
    elif 'mask' in k.lower():
      plt.imshow(v, cmap='Blues');
    else:
      plt.imshow(v, cmap=cmap)
      #plt.colorbar() # colorbar
    plt.title(k);
    plt.xticks([])
    plt.yticks([])
    plt.subplots_adjust(wspace=None, hspace=None)
  fig.tight_layout()

class S2_Dataset(Dataset):
    """ Derived Dataset class for loading imagery from an imset_dir."""

    def __init__(self, aoi_filename='data/aoi.csv', augment=True, crop_size=2, dropna=True, from_year=None, to_year=None):
        super().__init__()
        self.aoi_filename = pd.read_csv(aoi_filename)
        if dropna is True:
            self.aoi_filename = self.aoi_filename[self.aoi_filename["boat_index"]==self.aoi_filename["boat_index"]] # remove nans

        self.img_paths = self.aoi_filename['aoi_subdir'].values
        self.timestamp = self.aoi_filename['timestamp'].values
        self.lat = self.aoi_filename['lat'].values
        self.lon = self.aoi_filename['lon'].values
        self.y = self.aoi_filename['boat_index'].values

        if to_year is not None:
            idx = (self.timestamp <= str(to_year))
            self.img_paths = self.img_paths[idx]
            self.timestamp = self.timestamp[idx]
            self.y = self.y[idx]

        if from_year is not None:
            idx = (self.timestamp >= str(from_year))
            self.img_paths = self.img_paths[idx]
            self.timestamp = self.timestamp[idx]
            self.y = self.y[idx]

        self.band = "NIR_"
        self.augment = augment
        self.crop_size = crop_size # if self.augment is True
                    
    def __len__(self):
        return len(self.y)        

    def __getitem__(self, index):
        """    
        Builds a multi temporal, hyperspectral (NRGB) tensor from an S2 tile.
        Args:
            prefix: str, path to data
        Returns:
            imset: dict, an ImageSet for the given index (int).
        """
        
        if not isinstance(index, int):
            raise KeyError('index must be int')
            
        imset = {}
        filepath = self.img_paths[index]+'/'+self.band+self.timestamp[index]+'.png'
        #background_path = self.img_paths[index]+'/'+'NDWI_background.png'
        imset['filepath'] = filepath

        nir = imread(filepath) ##### NRG --> NIR
        #ndwi = imread(background_path) ##### 2 bands model (NIR + bk NDWI)
        imset['img'] = np.stack([nir/255.],0)
        imset['y'] = float(self.y[index])
        
        if self.augment is True:
            h_flip, v_flip = np.random.rand(1)>0.5, np.random.rand(1)>0.5 # random flip
            if v_flip:
                imset['img'] = imset['img'][:,::-1]
            if h_flip:
                imset['img'] = imset['img'][:,:,::-1]
            k = np.random.randint(4) # random rotate
            imset['img'] = np.rot90(imset['img'], k=k, axes=(1,2))
            crop_x = np.random.randint(0, self.crop_size)
            crop_y = np.random.randint(0, self.crop_size)
            imset['img'] = imset['img'][:, crop_x:-self.crop_size+crop_x, crop_y:-self.crop_size+crop_y]

        imset['img'] = torch.from_numpy(imset['img'])
        imset['y'] = torch.from_numpy(np.array([imset['y']]))
        return imset
